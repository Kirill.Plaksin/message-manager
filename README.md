Basic Auth by API
-
HEADER's:

Authorization: Token 'your token'

Content-Type: application/json

Tokens for urls bellow

admin : 9bd8e8c05f9061e80dd89bc7764e50fd4c56955b

user1 : cf5623131d5efe9f9898c325b21c1ff93900c1c4

user2: 9a52ddc2bb60d6ad67fbcaa171c0f523091501f7


URLS:
-
For creation short message

POST 

url: https://morning-chamber-02980.herokuapp.com/short-message/create/

Required field: body

Example body
`{"body": "Test body"}`

Example response `{
  "id": 7,
  "body": "Test body"
}`

For retrieve

GET

url: https://morning-chamber-02980.herokuapp.com/short-message/retrieve/(short_message_id)

example url: https://morning-chamber-02980.herokuapp.com/short-message/retrieve/7

Example response: 
`{
  "id": 7,
  "counter": 3,
  "body": "Test body"
}`

PATH

for update short message body

url: https://morning-chamber-02980.herokuapp.com/short-message/update/(short_message_id)

example url: https://morning-chamber-02980.herokuapp.com/short-message/update/7

Required field: body

Example body:
`{"body": "My new text"}`

Example response:
`{
  "id": 7,
  "counter": 0,
  "body": "My new text"
}`

DELETE

for delete short message

url: https://morning-chamber-02980.herokuapp.com/short-message/delete/(short_message_id)

example url: https://morning-chamber-02980.herokuapp.com/short-message/delete/7

How to deploy:
-
clone this repo than, 
login to heroku by `heroku login`
create project on heroku by `heroku create`
check if all right by `heroku local web`
set `heroku config:set DISABLE_COLLECTSTATIC=1`
and deploy application `git push heroku master`
after apply migrations `heroku run python manage.py migrate`
remember for creation user by `heroku run python manage.py createsuperuser`

After this steps you can create token in admin menu for your user, token will be need for creating/updating/deleting short message.

Local setup for run test 
- 
in your virtualenv

`pip install -r requirements.txt
`

`./manage.py test short_message.tests.tests_api_views.ShortMessageApiTestCase`

Tests
-
test available on short_message/tests/tests_api_views.py

Includes:
-
```
Django
Django REST framework
```

About
-
```
This is a simple web application.
Serves for creating/updating/retriving/deleting short messages.
```