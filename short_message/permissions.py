"""Base permissions."""
from rest_framework.permissions import BasePermission


class IsOwner(BasePermission):
    """Permission check object owner."""
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user
