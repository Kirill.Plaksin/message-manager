"""ShortMessage models."""
from django.db import models
from django.contrib.auth.models import User


class WhenMixin(models.Model):
    """Time stamp model."""
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class ShortMessage(WhenMixin):
    """Shor Message model."""
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    body = models.CharField(null=False, blank=False, max_length=160)
    counter = models.IntegerField(default=0)
