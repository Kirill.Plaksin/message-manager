from rest_framework import serializers

from short_message.models import ShortMessage


class ShortMessageSerializer(serializers.ModelSerializer):
    class Meta:
        read_only_fields = [
            'id',
            'counter',
        ]
        fields = read_only_fields + [
            'body',
        ]

        model = ShortMessage


class CreateShortMessageSerializer(serializers.ModelSerializer):
    class Meta:
        read_only_fields = ['id']
        fields = read_only_fields + ['body', ]
        model = ShortMessage
