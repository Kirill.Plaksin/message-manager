"""ShortMessage factory"""
import factory

from django.contrib.auth.models import User
from short_message.models import ShortMessage


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ('username',)

    username = 'john'


class ShortMessageFactory(factory.django.DjangoModelFactory):
    body = 'test text'
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = ShortMessage
