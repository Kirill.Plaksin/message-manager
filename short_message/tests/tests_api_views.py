"""Short message api test cases."""
from django.urls import reverse
from django.test import TestCase
from rest_framework.test import APIClient
from short_message.models import ShortMessage
from short_message.tests.factory import (
    UserFactory,
    ShortMessageFactory,
)


class ShortMessageApiTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.api_user = UserFactory()

    def test_create_short_message(self):
        create_url = reverse('short_message_creation')
        client = APIClient()
        client.force_authenticate(user=self.api_user)
        data = {'body': 'test text body'}
        response = client.post(create_url, data=data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()['body'], data['body'])
        short_message = ShortMessage.objects.first()
        self.assertEqual(short_message.user.id, self.api_user.id)
        self.assertEqual(short_message.body, data['body'])
        self.assertEqual(short_message.counter, 0)

    def test_retrieve_short_message_without_auth(self):
        short_message = ShortMessageFactory()
        self.assertEqual(short_message.counter, 0)
        client = APIClient()
        response = client.get(reverse('short_message_retrieve', kwargs={'pk': short_message.pk}))
        short_message.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(short_message.counter, 1)

    def test_path_short_message(self):
        short_message = ShortMessageFactory(counter=5, body='is my first text', user=self.api_user)
        data = {'body': 'is my second text'}
        client = APIClient()
        client.force_authenticate(user=self.api_user)
        response = client.patch(reverse('short_message_update', kwargs={'pk': short_message.id}), data=data)
        short_message.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(short_message.counter, 0)
        self.assertEqual(short_message.body, data['body'])

    def test_delete_short_message(self):
        short_message = ShortMessageFactory(user=self.api_user)
        client = APIClient()
        client.force_authenticate(user=self.api_user)
        response = client.delete(reverse('short_message_delete', kwargs={'pk': short_message.id}))
        self.assertEqual(response.status_code, 204)
        self.assertFalse(ShortMessage.objects.filter(pk=short_message.id).exists())

    def test_delete_short_message_by_not_owner(self):
        short_message = ShortMessageFactory(user=self.api_user)
        user = UserFactory(username='jan')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.delete(reverse('short_message_delete', kwargs={'pk': short_message.id}))
        self.assertEqual(response.status_code, 403)

    def test_update_short_message_by_not_owner(self):
        short_message = ShortMessageFactory(user=self.api_user)
        user = UserFactory(username='jan')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.patch(reverse('short_message_update', kwargs={'pk': short_message.id}))
        self.assertEqual(response.status_code, 403)

