from django.db import transaction
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    RetrieveAPIView,
    UpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
)
from rest_framework.response import Response
from rest_framework import status

from short_message.models import ShortMessage
from short_message.permissions import IsOwner
from short_message.serialazers import (
    CreateShortMessageSerializer,
    ShortMessageSerializer,
)


class RetrieveShortMessageApiViewSet(RetrieveAPIView):
    serializer_class = ShortMessageSerializer
    queryset = ShortMessage.objects.all()
    permission_classes = [AllowAny, ]

    @transaction.atomic
    def get(self, request, *args, **kwargs):
        short_message = self.get_object()
        short_message.counter += 1
        short_message.save()
        serializer = ShortMessageSerializer(short_message)
        return Response(serializer.data, status=status.HTTP_200_OK)


class CreateShortMessageApiViewSet(CreateAPIView):
    authentication_classes = (TokenAuthentication, )
    serializer_class = CreateShortMessageSerializer
    permission_classes = [IsAuthenticated, ]
    queryset = ShortMessage.objects.all()

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = CreateShortMessageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=self.request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)


class UpdateShortMessageApiViewSet(UpdateAPIView):
    authentication_classes = (TokenAuthentication, )
    permission_classes = [IsAuthenticated, IsOwner]
    serializer_class = ShortMessageSerializer
    queryset = ShortMessage.objects.all()

    def put(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    @transaction.atomic
    def patch(self, request, *args, **kwargs):
        short_message = self.get_object()
        short_message.counter = 0
        short_message.save()
        serializer = ShortMessageSerializer(short_message, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DestroyShortMessageApiViewSet(DestroyAPIView):
    authentication_classes = (TokenAuthentication, )
    queryset = ShortMessage.objects.all()
    permission_classes = [IsAuthenticated, IsOwner]

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        short_message = self.get_object()
        short_message.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
