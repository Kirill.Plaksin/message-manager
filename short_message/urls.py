from django.urls import path
from short_message import views


urlpatterns = [
    path('create/', views.CreateShortMessageApiViewSet.as_view(), name='short_message_creation'),
    path('retrieve/<int:pk>', views.RetrieveShortMessageApiViewSet.as_view(), name='short_message_retrieve'),
    path('update/<int:pk>', views.UpdateShortMessageApiViewSet.as_view(), name='short_message_update'),
    path('delete/<int:pk>', views.DestroyShortMessageApiViewSet.as_view(), name='short_message_delete'),
]
